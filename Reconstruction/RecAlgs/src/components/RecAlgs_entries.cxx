#include "../TimingAlg.h"
#include "../MemoryAlg.h"
#include "../JobInfo.h"
#include "../EventInfoUnlocker.h"
#include "../AppStopAlg.h"

DECLARE_COMPONENT( TimingAlg )
DECLARE_COMPONENT( MemoryAlg )
DECLARE_COMPONENT( JobInfo )
DECLARE_COMPONENT( EventInfoUnlocker )
DECLARE_COMPONENT( AppStopAlg )

