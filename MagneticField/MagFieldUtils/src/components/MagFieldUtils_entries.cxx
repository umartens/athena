#include "MagFieldUtils/MagFieldTestbedAlg.h"
#include "MagFieldUtils/SolenoidTest.h"
#include "MagFieldUtils/IdentityManipulator.h"

DECLARE_COMPONENT( MagField::MagFieldTestbedAlg )
DECLARE_COMPONENT( MagField::SolenoidTest )
DECLARE_COMPONENT( MagField::IdentityManipulator )

