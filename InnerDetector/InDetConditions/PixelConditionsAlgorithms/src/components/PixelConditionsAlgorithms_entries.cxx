#include "../PixelDCSCondHVAlg.h"
#include "../PixelDCSCondTempAlg.h"
#include "../PixelDCSCondStateAlg.h"
#include "../PixelConfigCondAlg.h"
#include "../PixelChargeCalibCondAlg.h"
#include "../PixelTDAQCondAlg.h"
#include "../PixelSiliconConditionsTestAlg.h"
#include "../SpecialPixelMapCondAlg.h"
#include "../PixelOfflineCalibCondAlg.h"

DECLARE_COMPONENT( PixelDCSCondHVAlg )
DECLARE_COMPONENT( PixelDCSCondTempAlg )
DECLARE_COMPONENT( PixelDCSCondStateAlg )
DECLARE_COMPONENT( PixelConfigCondAlg )
DECLARE_COMPONENT( PixelChargeCalibCondAlg )
DECLARE_COMPONENT( PixelTDAQCondAlg )
DECLARE_COMPONENT( PixelSiliconConditionsTestAlg )
DECLARE_COMPONENT( SpecialPixelMapCondAlg )
DECLARE_COMPONENT( PixelOfflineCalibCondAlg )
